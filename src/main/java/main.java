import java.lang.reflect.Array;
import java.util.*;
import java.util.regex.*;


public class KWIC {
    public String[] ignoreWords;
    public String[] titles;
    public String[] keywords;

    //Find ignore words;
    public static void getIgnoredAndTitles(String input) {
        String[] splitArr = input.split("::");
        ignoreWords = splitArr[0].split("\n");
        titles = splitArr[1].split("\n");
    }

    public static void getKeyWords(String input){
        getIgnoredAndTitles(input);
        for (String title: titles){
            String[] words = title.split(" ");
            for (String word: words) {
                if (!ignoreWords.contains(word)){
                   keywords.add(word.toLowerCase());
            }
        }
            keywords = sorted(keywords);
        }
    }

    public static String getKWIC(String input){
        getKeyWords(input);
        StringBuilder sb = new StringBuilder();
        for (String keyword: keywords){
            for (String title : titles){
                ArrayList<String> words= new ArrayList<>(List.of(title));
                for (String word: words){
                    if (word.equals(keyword)){
                        sb.append(word.toUpperCase(Locale.ROOT));
                    }
                    else {
                        sb.append(word);
                    }
                    sb.append(" ");
                }
                sb.append("\n");
            }
        }
        return sb.toString();
    }
}


