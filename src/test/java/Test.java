import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class KWIC {
    @Test
    void test1() {
        String txt = "is\n" +
                "the\n" +
                "of\n" +
                "and\n" +
                "as\n" +
                "a\n" +
                "but\n" +
                "::\n" +
                "Descent of Man\n" +
                "The Ascent of Man\n" +
                "The Old Man and The Sea\n" +
                "A Portrait of The Artist As a Young Man\n" +
                "A Man is a Man but Bubblesort IS A DOG";
        String expected = "a portrait of the ARTIST as a young man\n" +
                "the ASCENT of man\n" +
                "a man is a man but BUBBLESORT is a dog\n" +
                "DESCENT of man\n" +
                "a man is a man but bubblesort is a DOG\n" +
                "descent of MAN\n" +
                "the ascent of MAN\n" +
                "the old MAN and the sea\n" +
                "a portrait of the artist as a young MAN\n" +
                "a MAN is a man but bubblesort is a dog\n" +
                "a man is a MAN but bubblesort is a dog\n" +
                "the OLD man and the sea\n" +
                "a PORTRAIT of the artist as a young man\n" +
                "the old man and the SEA\n" +
                "a portrait of the artist as a YOUNG man";
        assertEquals(new kwic().kwic(txt), expected);
    }

    @Test
    void testGetIgnoredAndTitles(){
        KWIC kwic = new KWIC();
        String txt = "is\n" +
                "the\n" +
                "of\n" +
                "and\n" +
                "as\n" +
                "a\n" +
                "but\n" +
                "::\n" +
                "Descent of Man\n" +
                "The Ascent of Man\n" +
                "The Old Man and The Sea\n" +
                "A Portrait of The Artist As a Young Man\n" +
                "A Man is a Man but Bubblesort IS A DOG";
        kwic.getIgnoredAndTitles(txt);

        assertArrayEquals(new String[]{"is", "the", "of", "and", "as", "a", "but"}, kwic.ignoreWords);
        assertArrayEquals(new String[]{ "Descent of Man",
                                        "The Ascent of Man",
                                        "The Old Man and The Sea",
                                        "A Portrait of The Artist As a Young Man",
                                        "A Man is a Man but Bubblesort IS A DOG"}, kwic.titles);
    }

    @Test
    static void testGetKeyWords(){
        KWIC kwic = new KWIC();
        String txt = "is\n" +
                "the\n" +
                "of\n" +
                "and\n" +
                "as\n" +
                "a\n" +
                "but\n" +
                "::\n" +
                "Descent of Man\n" +
                "The Ascent of Man\n" +
                "The Old Man and The Sea\n";
        kwic.getKeyWords(txt);
        assertArrayEquals(new String[]{"ascent", "descent", "man", "old", "sea"}, kwic.keywords);
    }

    @Test
    static void testGetKWIC(){
        KWIC kwic = new KWIC();
        String txt = "is\n" +
                "the\n" +
                "of\n" +
                "and\n" +
                "as\n" +
                "a\n" +
                "but\n" +
                "::\n" +
                "Descent of Man\n" +
                "The Ascent of Man\n" +
                "The Old Man and The Sea\n";
        kwic.getIgnoredAndTitles(txt);
        String output = kwic.getKWIC(txt);
        String answer = "the ASCENT of man\n" +
                        "DESCENT of man\n" +
                        "the ascent of MAN\n" +
                        "descent of MAN\n" +
                        "the old MAN and the sea\n" +
                        "the OLD man and the sea\n" +
                        "the old man and the SEA\n";
        assertEquals(answer, output);
    }
}

